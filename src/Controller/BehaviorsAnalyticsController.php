<?php

namespace Drupal\behaviors_analytics\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Behaviors Analytics routes.
 */
class BehaviorsAnalyticsController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
